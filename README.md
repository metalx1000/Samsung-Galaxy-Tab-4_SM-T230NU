# Samsung-Galaxy-Tab-4_SM-T230NU

- Prerequisites
```bash
sudo apt install heimdall-flash
```

- Reboot to Bootloader
Reboot while holding VolumeDown+Home buttons
Then Press VolumeUp

- Reboot into Recovery
Reboot while holding VolumeUp+Home buttons

# Install TWRP
```bash
heimdall flash --RECOVERY twrp/recovery.img  --no-reboot
```
https://forum.xda-developers.com/t/twrp-3-2-1-0-official-samsung-galaxy-tab-4-sm-t230nu-latest.3761221/
Boot into recovery before you boot the system 
(booting the system before going into TWRP might over right TWRP)

# Flash Custom ROM from TWRP
https://forum.xda-developers.com/t/sm-t230nu-cosaromv2.4417247/
Boot into TWRP
```bash
adb push CosaROMv2/CosaROMv2.zip /sdcard
```
The on Device Choose install and select the zip file

# Flash STOCK ROM
https://firmwarefile.com/samsung-sm-t230nu
```bash
heimdall flash --SYSTEM stockrom/system.img --no-reboot
heimdall flash --RECOVERY stockrom/recovery.img --no-reboot
```
```bash
#continue with each partition you want to flash
cd stockrom
heimdall flash --SBL1 sbl1.mbn --DBI sdi.mbn --TZ tz.mbn --RPM rpm.mbn --ABOOT aboot.mbn --BOOT boot.img --RECOVERY recovery.img --APNHLOS NON-HLOS.bin --SYSTEM system.img.raw --HIDDEN hidden.img.raw --CACHE cache.img.raw --MODEM modem.bin --no-reboot


```
